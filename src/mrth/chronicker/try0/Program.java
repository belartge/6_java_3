package mrth.chronicker.try0;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

public class Program {

   public static void main(String[] args) {
      Tree<Integer> tree = new Tree<>();
      tree.insertToTree(3);
      tree.insertToTree(4);
      tree.insertToTree(6);
      tree.insertToTree(5);

      OutputStream outputStream = new PrintStream(System.out);
      OutputStreamWriter writer = new OutputStreamWriter(System.out);

      try {
         tree.printTree(writer);
      } catch (IOException ex) {
         ex.printStackTrace();
      }
   }

}

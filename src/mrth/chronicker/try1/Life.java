package mrth.chronicker.try1;

import java.util.List;
import java.util.Random;

public abstract class Life {

   protected Random random = new Random();

   protected String name;
   protected String vid;

   public abstract void eat(Life life);
   public abstract Life spread(List<Life> parents);

   public abstract String getName();
}

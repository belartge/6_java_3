package mrth.chronicker.try1.fauna;

import mrth.chronicker.try1.Life;

import java.util.HashSet;
import java.util.Set;

public abstract class Fauna extends Life {

   private Double velocity;

   protected Set<WayOfMoving> waysOfMoving = new HashSet<>();
   public abstract void move();

   public String getNameOfMoving() {


      int size = waysOfMoving.size();
      int item = random.nextInt(size); // In real life, the Random object should be rather more shared than this
      int i = 0;
      WayOfMoving wayOfMoving = waysOfMoving.iterator().next();
      for(WayOfMoving obj : waysOfMoving)
      {
         if (i == item)
            wayOfMoving = obj;
         i++;
      }

      switch (wayOfMoving) {
         case FLY: return "летит";
         case SWIM: return "плывет";
         case RUN: return "бежит";
         case WALK: return "идет";
      }
      return "бездействует";
   }
}

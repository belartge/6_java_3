package mrth.chronicker.try1.fauna;

public interface Predatory {
   public void eat(Fauna animal);
}
